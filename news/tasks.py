from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger

from .utils import fetch_save_latest_news

logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(minute='*/1')),
    name="task_fetch_latest_news",
    ignore_result=True
)
def task_fetch_latest_news():
    """
    Fetch latest news from online news.
    """
    print("Fetch news task is called !!")
    fetch_save_latest_news()    
    logger.info("Updated news to the database from online news !!")

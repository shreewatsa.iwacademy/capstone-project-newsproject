from django.db import models

# Create your models here.
class News(models.Model):
    headline = models.CharField(max_length=100)
    content = models.TextField()
    publish_date = models.DateTimeField(auto_now=True)
    reporter = models.CharField( max_length=100, null=True, blank=True)

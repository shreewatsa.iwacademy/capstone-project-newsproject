from .models import News
import requests
import urllib.request 
import time
from bs4 import BeautifulSoup

def fetch_save_latest_news():
    print("Fetch news task function was actually called !!")

    url = 'https://www.kantipurdaily.com/'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')

    articles = soup.findAll('article')
    # print(articles[0])
    i = 0
    for article in reversed(articles):
        try:
            headline = article.h2.a.text
            summary = article.p.text
            # News.objects.create(headline = headline, content=summary)
            news = News()
            news.headline = headline
            news.content = summary
            news.save()
            print(i,')' , headline)
            i += 1
        except:
            print("Some errors occurred while saving news to database !!")
            pass

from django.shortcuts import render

from .models import News

# Create your views here.
def view_news(request):
    total_news = News.objects.count()
    allNews = News.objects.all().order_by("-publish_date")
    toBeDeleted = total_news - 10
    if toBeDeleted > 0:
        News.objects.filter(pk__in=list(News.objects.all().order_by('publish_date')[:toBeDeleted].values_list('id',flat=True))).delete()
    return render(request, 'news/news.html', {'allNews':allNews})